# models-on-a-plane

## Deployment

Deployed to CloudFlare Pages @ https://models-on-a-plane.pages.dev

## TODO (privacy process) & disclaimer

This website attempts to implement privacy features that by manual allowance that is new and untested, and should not be trusted. If you try to reflect details here, you should do your own very careful testing. This does not even fit my own needs yet, and the website is not hosted.

- URL -> `data-domain` in `plausible.html`
- URL -> `quarto-ojs-runtime.js`, consider also overwriting `observableusercontent` link?
- controls around keeping and maintaining `_site/resources_vz`

## Purpose

This will be a first hosted gallery for interactive and animated calculang models \o/

It will probably be temporary, being replaced by my own website and a more official calculang/documentation site.

Baby steps!

## Models

I'll release [these](https://twitter.com/calculang/status/1669775946671423493) models here, and perhaps some others.

## Stack (incomplete):

- `vz` a private tool (public very soon)
- calculang [introspection](https://github.com/calculang/calculang/tree/main/packages/introspection-api#readme) json
- [Quarto](https://quarto.org/) and
- [Observable runtime](https://github.com/observablehq/runtime)
- [UW IDL](https://github.com/uwdata) visualisation stack including Vega-Lite, Gemini
- Graphviz
- d3 (**help-wanted**)

## Caveats / Notes

- All models may not have been made on a plane, but...
- all models are a little disheveled, as though they were made on a plane.

## Feedback

This is a demo site, feedback welcome!
