// based on Haskell model from
// https://byorgey.wordpress.com/2023/05/31/competitive-programming-in-haskell-introduction-to-dynamic-programming/

// m function:

export const l = () => l_in;
export const r = () => r_in;

export const m = () => {
  if (l() == "(" && r() == ")") return 1;
  if (l() == "[" && r() == "]") return 1;
  if (l() == "{" && r() == "}") return 1;
  if (l() == "?" && r() == "?") return 3;

  if ("([{".includes(l()) && r() == "?") return 1;
  if (")]}".includes(r()) && l() == "?") return 1;

  // everything else:
  return 0;
};

// c function:

export const i = () => i_in;
export const j = () => j_in;

export const c = () => {
  if (i() == j()) return 1;
  if (i() % 2 != j() % 2) return 0;

  return [...Array(j() - 1 + 1).keys()] /*.filter(k => k >= i() + 1)*/
    .reduce((a, k) => {
      if (k < i() + 1 || (k - i()) % 2 == 0) return a;
      // summation rules
      //(k >= i() + 1) * // UNCERTAIN how all of this will behave in different cases !!
      //  ((k - i() - 1) % 2 == 1) *
      // summand
      //k = k - 1;
      return (
        m({ l_in: s({ str_i_in: i() }), r_in: s({ str_i_in: k }) }) *
          c({ i_in: i() + 1, j_in: k }) *
          c({ i_in: k + 1 /*, j_in:j()*/ }) +
        a
      );
    }, 0);
};

export const str = () => str_in;
export const str_i = () => str_i_in;

export const s = () => str()[str_i()];

export const solve = () => c({ i_in: 0, j_in: str().length });

// todo format!!

// viz specs?
