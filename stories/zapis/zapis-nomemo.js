(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory();
	else if(typeof define === 'function' && define.amd)
		define([], factory);
	else {
		var a = factory();
		for(var i in a) (typeof exports === 'object' ? exports : root)[i] = a[i];
	}
})(this, function() {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "l", function() { return l; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "r", function() { return r; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "m", function() { return m; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "i", function() { return i; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "j", function() { return j; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return c; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "str", function() { return str; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "str_i", function() { return str_i; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "s", function() { return s; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "solve", function() { return solve; });
// based on Haskell model from
// https://byorgey.wordpress.com/2023/05/31/competitive-programming-in-haskell-introduction-to-dynamic-programming/

// m function:

const l = ({ l_in }) => l_in;
const r = ({ r_in }) => r_in;

const m = ({ l_in, r_in }) => {
  if (l({ l_in }) == "(" && r({ r_in }) == ")") return 1;
  if (l({ l_in }) == "[" && r({ r_in }) == "]") return 1;
  if (l({ l_in }) == "{" && r({ r_in }) == "}") return 1;
  if (l({ l_in }) == "?" && r({ r_in }) == "?") return 3;

  if ("([{".includes(l({ l_in })) && r({ r_in }) == "?") return 1;
  if (")]}".includes(r({ r_in })) && l({ l_in }) == "?") return 1;

  // everything else:
  return 0;
};

// c function:

const i = ({ i_in }) => i_in;
const j = ({ j_in }) => j_in;

const c = ({ i_in, j_in, l_in, r_in, str_in, str_i_in }) => {
  if (i({ i_in }) == j({ j_in })) return 1;
  if (i({ i_in }) % 2 != j({ j_in }) % 2) return 0;

  return [...Array(j({ j_in }) - 1 + 1).keys()] /*.filter(k => k >= i() + 1)*/.
  reduce((a, k) => {
    if (k < i({ i_in }) + 1 || (k - i({ i_in })) % 2 == 0) return a;
    // summation rules
    //(k >= i() + 1) * // UNCERTAIN how all of this will behave in different cases !!
    //  ((k - i() - 1) % 2 == 1) *
    // summand
    //k = k - 1;
    return (
      m({ l_in: s({ str_in, str_i_in: i({ i_in }) }), r_in: s({ str_in, str_i_in: k }) }) *
      c({ l_in, r_in, str_in, str_i_in, i_in: i({ i_in }) + 1, j_in: k }) *
      c({ j_in, l_in, r_in, str_in, str_i_in, i_in: k + 1 /*, j_in:j()*/ }) +
      a);

  }, 0);
};

const str = ({ str_in }) => str_in;
const str_i = ({ str_i_in }) => str_i_in;

const s = ({ str_in, str_i_in }) => str({ str_in })[str_i({ str_i_in })];

const solve = ({ l_in, r_in, str_in, str_i_in }) => c({ l_in, r_in, str_in, str_i_in, i_in: 0, j_in: str({ str_in }).length });

// todo format!!

// viz specs?

/***/ })
/******/ ]);
});
//# sourceMappingURL=zapis-nomemo.js.map