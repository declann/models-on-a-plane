
import { memoize } from 'underscore';
//import memoize from 'lru-memoize';
//import { isEqual } from 'underscore'; // TODO poor tree shaking support, or why is this impact so massive? Move to lodash/lodash-es?

import { l_ as l$, r_ as r$, m_ as m$, i_ as i$, j_ as j$, c_ as c$, str_ as str$, str_i_ as str_i$, s_ as s$, solve_ as solve$ } from "./cul_scope_1.mjs"; // there is already-culed stuff in here, why? imports to memo loader include cul_scope_id, what logic should it apply RE passing forward? eliminate? Probably!



////////// start l memo-loader code //////////
//const l$m = memoize(999999, isEqual)(l$);
export const l$m = memoize(l$, JSON.stringify);
export const l = (a) => {
  return l$m(a);
  // eslint-disable-next-line no-undef
  l$({ l_in }); // never run, but here to "trick" calculang graph logic
};
////////// end l memo-loader code //////////



////////// start r memo-loader code //////////
//const r$m = memoize(999999, isEqual)(r$);
export const r$m = memoize(r$, JSON.stringify);
export const r = (a) => {
  return r$m(a);
  // eslint-disable-next-line no-undef
  r$({ r_in }); // never run, but here to "trick" calculang graph logic
};
////////// end r memo-loader code //////////



////////// start m memo-loader code //////////
//const m$m = memoize(999999, isEqual)(m$);
export const m$m = memoize(m$, JSON.stringify);
export const m = (a) => {
  return m$m(a);
  // eslint-disable-next-line no-undef
  m$({ l_in, r_in }); // never run, but here to "trick" calculang graph logic
};
////////// end m memo-loader code //////////



////////// start i memo-loader code //////////
//const i$m = memoize(999999, isEqual)(i$);
export const i$m = memoize(i$, JSON.stringify);
export const i = (a) => {
  return i$m(a);
  // eslint-disable-next-line no-undef
  i$({ i_in }); // never run, but here to "trick" calculang graph logic
};
////////// end i memo-loader code //////////



////////// start j memo-loader code //////////
//const j$m = memoize(999999, isEqual)(j$);
export const j$m = memoize(j$, JSON.stringify);
export const j = (a) => {
  return j$m(a);
  // eslint-disable-next-line no-undef
  j$({ j_in }); // never run, but here to "trick" calculang graph logic
};
////////// end j memo-loader code //////////



////////// start c memo-loader code //////////
//const c$m = memoize(999999, isEqual)(c$);
export const c$m = memoize(c$, JSON.stringify);
export const c = (a) => {
  return c$m(a);
  // eslint-disable-next-line no-undef
  c$({ i_in, j_in, l_in, r_in, str_in, str_i_in }); // never run, but here to "trick" calculang graph logic
};
////////// end c memo-loader code //////////



////////// start str memo-loader code //////////
//const str$m = memoize(999999, isEqual)(str$);
export const str$m = memoize(str$, JSON.stringify);
export const str = (a) => {
  return str$m(a);
  // eslint-disable-next-line no-undef
  str$({ str_in }); // never run, but here to "trick" calculang graph logic
};
////////// end str memo-loader code //////////



////////// start str_i memo-loader code //////////
//const str_i$m = memoize(999999, isEqual)(str_i$);
export const str_i$m = memoize(str_i$, JSON.stringify);
export const str_i = (a) => {
  return str_i$m(a);
  // eslint-disable-next-line no-undef
  str_i$({ str_i_in }); // never run, but here to "trick" calculang graph logic
};
////////// end str_i memo-loader code //////////



////////// start s memo-loader code //////////
//const s$m = memoize(999999, isEqual)(s$);
export const s$m = memoize(s$, JSON.stringify);
export const s = (a) => {
  return s$m(a);
  // eslint-disable-next-line no-undef
  s$({ str_in, str_i_in }); // never run, but here to "trick" calculang graph logic
};
////////// end s memo-loader code //////////



////////// start solve memo-loader code //////////
//const solve$m = memoize(999999, isEqual)(solve$);
export const solve$m = memoize(solve$, JSON.stringify);
export const solve = (a) => {
  return solve$m(a);
  // eslint-disable-next-line no-undef
  solve$({ l_in, r_in, str_in, str_i_in }); // never run, but here to "trick" calculang graph logic
};
////////// end solve memo-loader code //////////