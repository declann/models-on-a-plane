export const x = () => {
  if (t() == 0) return 100 + dx();
  else return x({ t_in: t() - 1 }) + dx();
};

export const dx = () => dx_in;

export const y = () => {
  if (t() == 0) return 50;
  else return y({ t_in: t() - 1 }) + dy({ t_in: t() - 1 });
};

export const dy = () => { // y velocity
  if (t() == 0) return 3;
  else if (y({ t_in: t() }) > 185)
    return -dy({ t_in: t() - 1 }); // bounce at the floor by negating dy
  else return dy({ t_in: t() - 1 });
};

export const t = () => t_in;
