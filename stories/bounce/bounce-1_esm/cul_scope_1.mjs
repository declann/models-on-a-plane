import { t } from "./cul_scope_0.mjs";import { y } from "./cul_scope_0.mjs";import { dx } from "./cul_scope_0.mjs";import { x } from "./cul_scope_0.mjs";export const x_ = ({ t_in, dx_in }) => {
  if (t({ t_in }) == 0) return 100 + dx({ dx_in });else
  return x({ dx_in, t_in: t({ t_in }) - 1 }) + dx({ dx_in });
};

export const dx_ = ({ dx_in }) => dx_in;

export const y_ = ({}) => 50;

export const t_ = ({ t_in }) => t_in;