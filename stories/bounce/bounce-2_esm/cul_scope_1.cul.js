export const x = () => {
  if (t() == 0) return 100 + dx();
  else return x({ t_in: t() - 1 }) + dx();
};

export const dx = () => dx_in;

export const y = () => {
  if (t() == 0) return 50;
  else return y({ t_in: t() - 1 }) + dy({ t_in: t() - 1 });
};

export const dy = () => 3;

export const t = () => t_in;
