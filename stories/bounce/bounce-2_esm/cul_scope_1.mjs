import { t } from "./cul_scope_0.mjs";import { dy } from "./cul_scope_0.mjs";import { y } from "./cul_scope_0.mjs";import { dx } from "./cul_scope_0.mjs";import { x } from "./cul_scope_0.mjs";export const x_ = ({ t_in, dx_in }) => {
  if (t({ t_in }) == 0) return 100 + dx({ dx_in });else
  return x({ dx_in, t_in: t({ t_in }) - 1 }) + dx({ dx_in });
};

export const dx_ = ({ dx_in }) => dx_in;

export const y_ = ({ t_in }) => {
  if (t({ t_in }) == 0) return 50;else
  return y({ t_in: t({ t_in }) - 1 }) + dy({ t_in: t({ t_in }) - 1 });
};

export const dy_ = ({}) => 3;

export const t_ = ({ t_in }) => t_in;