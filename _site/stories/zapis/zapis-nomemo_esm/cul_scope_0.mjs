// based on Haskell model from
// https://byorgey.wordpress.com/2023/05/31/competitive-programming-in-haskell-introduction-to-dynamic-programming/

// m function:

export const l = ({ l_in }) => l_in;
export const r = ({ r_in }) => r_in;

export const m = ({ l_in, r_in }) => {
  if (l({ l_in }) == "(" && r({ r_in }) == ")") return 1;
  if (l({ l_in }) == "[" && r({ r_in }) == "]") return 1;
  if (l({ l_in }) == "{" && r({ r_in }) == "}") return 1;
  if (l({ l_in }) == "?" && r({ r_in }) == "?") return 3;

  if ("([{".includes(l({ l_in })) && r({ r_in }) == "?") return 1;
  if (")]}".includes(r({ r_in })) && l({ l_in }) == "?") return 1;

  // everything else:
  return 0;
};

// c function:

export const i = ({ i_in }) => i_in;
export const j = ({ j_in }) => j_in;

export const c = ({ i_in, j_in, l_in, r_in, str_in, str_i_in }) => {
  if (i({ i_in }) == j({ j_in })) return 1;
  if (i({ i_in }) % 2 != j({ j_in }) % 2) return 0;

  return [...Array(j({ j_in }) - 1 + 1).keys()] /*.filter(k => k >= i() + 1)*/.
  reduce((a, k) => {
    if (k < i({ i_in }) + 1 || (k - i({ i_in })) % 2 == 0) return a;
    // summation rules
    //(k >= i() + 1) * // UNCERTAIN how all of this will behave in different cases !!
    //  ((k - i() - 1) % 2 == 1) *
    // summand
    //k = k - 1;
    return (
      m({ l_in: s({ str_in, str_i_in: i({ i_in }) }), r_in: s({ str_in, str_i_in: k }) }) *
      c({ l_in, r_in, str_in, str_i_in, i_in: i({ i_in }) + 1, j_in: k }) *
      c({ j_in, l_in, r_in, str_in, str_i_in, i_in: k + 1 /*, j_in:j()*/ }) +
      a);

  }, 0);
};

export const str = ({ str_in }) => str_in;
export const str_i = ({ str_i_in }) => str_i_in;

export const s = ({ str_in, str_i_in }) => str({ str_in })[str_i({ str_i_in })];

export const solve = ({ l_in, r_in, str_in, str_i_in }) => c({ l_in, r_in, str_in, str_i_in, i_in: 0, j_in: str({ str_in }).length });

// todo format!!

// viz specs?