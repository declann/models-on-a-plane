
viewof field = {}

model = require('./bounce-0.js');

introspection = await FileAttachment('./bounce-0-nomemo.introspection.json').json({typed:true});

cul_0 = await FileAttachment('./bounce-0-nomemo_esm/cul_scope_0.cul.js').text();
esm_0 = await FileAttachment('./bounce-0-nomemo_esm/cul_scope_0.mjs').text();

descs = ["fixed ball @ (100,50) ⚽","add x velocity ➡️","add y velocity ↘️", "bounce off floor ⛹️‍♂️", "add y acceleration 🏎️", "floor alignment 🔧", "dampen y energy 🏆"]


spec = ({
  mark: 'point',
  height: 400,
  channels: {
    x: {'name': 'x', type:'quantitative'},
    y: {'name': 'y', type:'quantitative'},
    detail: {'name': 't_in', type:'nominal'},
    color: {'name': 't_in', type:'nominal'},
    //row: 'interaction',
    //color: 'interaction'
  }
})


gemSpec = ({
      //"meta": {"name": "All at once"},
      "timeline": {
        "sync": [
          /*{
            "component": {"axis": "x"},
            "timing": {"duration": {"ratio": 1}}
          },
          {
            "component": {"axis": "y"},
            "timing": {"duration": {"ratio": 1}}
          },*/
          {
            "component": {"mark": "marks"},
            "change": {"data": ["t_in"]},
            "timing": {"duration": {"ratio": 1}}
          },
        ]
      },
      "totalDuration": 600
    })


function spec_post_process (spec) {
  let t = {...spec}
  t["config"] ={"legend": {"disable": true}}
  if (t.encoding.x) t.encoding.x.axis = {labelAngle: 0, orient:"top"}
  if (t.encoding.y.field == "y" && 1) { t.encoding.y.sort = "descending"}; 
  if (t.encoding.y.scale != undefined) {
  t.encoding.y.scale.zero = false; }
  if (t.encoding.x.scale != undefined) {t.encoding.x.scale.zero = false}

  t.encoding.x.scale = {zero: false, domain: [80,300]}
  t.encoding.y.scale = {zero: false, domain: [10,230]}



  return t
}

domains = ({
  t_in: _.range(ui.t_interval[0],ui.t_interval[1]),

})

uis =({ // if not mapped
  dampener_in: Inputs.range([0.5,1.1], {value: 0.90, step:0.005, label: "y dampener ✖", disabled:!inputs.includes('dampener_in') }),
  dx_in: Inputs.range([-5,5], {value: 3, step:0.1, label: "dx Δ", disabled:!inputs.includes('dx_in')}),
  t_interval: interval([0,100], {step:1, label: 'time ↔️', color: 'skyblue', value:[0,60]}),
})

inputs_default = [{dampener_in:0.9,dx_in:3,t_interval:[0,60]}] // broken first interaction if not set consistent with uis defaults!

// how to make t interval changes not affect input_history (b/c affect everyhing)

/////////////////////////////

inputs = Object.values(introspection.cul_functions).filter(d => d.reason == 'input definition').map(d => d.name)

// must be here to permit dynamic domains
viewof ui = {
  let f = {};
  Object.keys(uis).filter(d => d != "default" && uis[d] != undefined).forEach(k => {
    if (!mapped.includes(k)) { // including all UIs except default, but this needs thought
      f[k] = uis[k];
      // weird rendering/updating errors using this approach to disable:
  /*if (f[k][0] != undefined) {
    let myinputs = Object.values(introspection.cul_functions).filter(d => d.reason == 'input definition').map(d => d.name)
    if (!(myinputs.includes(k))) {
      //console.log(inputs)
      f[k][0].disabled = true
      f[k][1].disabled = true
    }
    }*/
  }
  })
  let form = Inputs.form(f);
  let state = false;
  form.oninput = () => {if (state == false) {if(mapped.includes('interaction')) mutable inputs_history = [...mutable inputs_history, form.value]; state = true}}
  form.onchange = () => { state = false; mutable inputs_history[mutable inputs_history.length-1] = form.value  };
  //if(mapped.includes('interaction')) mutable inputs_history = [...mutable inputs_history, form.value]
  return form
}

mapped = [...new Set(Object.values(spec.channels).map(d => d.name??d))].filter(d => d != 'value')



/// separate this todo

// manually bringing interval slider in
// the code in this cell is Copyright 2023 Fabian Iwand under the following license:

/*
Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/



function interval(range = [], options = {}) {
  const [min = 0, max = 1] = range;
  const {
    step = .001,
    label = null,
    value = [min, max],
    format = ([start, end]) => `${start} … ${end}`,
    color,
    width,
    theme,
  } = options;

  const __ns__ = DOM.uid('scope').id;
  const css = `
#${__ns__} {
  font: 13px/1.2 var(--sans-serif);
  display: flex;
  align-items: baseline;
  flex-wrap: wrap;
  max-width: 100%;
  width: auto;
}
@media only screen and (min-width: 30em) {
  #${__ns__} {
    flex-wrap: nowrap;
    width: 360px;
  }
}
#${__ns__} .label {
  width: 120px;
  padding: 5px 0 4px 0;
  margin-right: 6.5px;
  flex-shrink: 0;
}
#${__ns__} .form {
  display: flex;
  width: 100%;
}
#${__ns__} .range {
  flex-shrink: 1;
  width: 100%;
}
#${__ns__} .range-slider {
  width: 100%;
}
  `;
  
  const $range = rangeInput({min, max, value: [value[0], value[1]], step, color, width, theme});
  const $output = html`<output>`;
  const $view = html`<div id=${__ns__}>
${label == null ? '' : html`<div class="label">${label}`}
<div class=form>
  <div class=range>
    ${$range}<div class=range-output>${$output}</div>
  </div>
</div>
${html`<style>${css}`}
  `;

  const update = () => {
    const content = format([$range.value[0], $range.value[1]]);
    if(typeof content === 'string') $output.value = content;
    else {
      while($output.lastChild) $output.lastChild.remove();
      $output.appendChild(content);
    }
  };
  $range.oninput = update;
  update();
  
  return Object.defineProperty($view, 'value', {
    get: () => $range.value,
    set: ([a, b]) => {
      $range.value = [a, b];
      update();
    },
  });
}


theme_Flat = `
/* Options */
:scope {
  color: #3b99fc;
  width: 240px;
}

:scope {
  position: relative;
  display: inline-block;
  --thumb-size: 15px;
  --thumb-radius: calc(var(--thumb-size) / 2);
  padding: var(--thumb-radius) 0;
  margin: 2px;
  vertical-align: middle;
}
:scope .range-track {
  box-sizing: border-box;
  position: relative;
  height: 7px;
  background-color: hsl(0, 0%, 80%);
  overflow: visible;
  border-radius: 4px;
  padding: 0 var(--thumb-radius);
}
:scope .range-track-zone {
  box-sizing: border-box;
  position: relative;
}
:scope .range-select {
  box-sizing: border-box;
  position: relative;
  left: var(--range-min);
  width: calc(var(--range-max) - var(--range-min));
  cursor: ew-resize;
  background: currentColor;
  height: 7px;
  border: inherit;
}
/* Expands the hotspot area. */
:scope .range-select:before {
  content: "";
  position: absolute;
  width: 100%;
  height: var(--thumb-size);
  left: 0;
  top: calc(2px - var(--thumb-radius));
}
:scope .range-select:focus,
:scope .thumb:focus {
  outline: none;
}
:scope .thumb {
  box-sizing: border-box;
  position: absolute;
  width: var(--thumb-size);
  height: var(--thumb-size);

  background: #fcfcfc;
  top: -4px;
  border-radius: 100%;
  border: 1px solid hsl(0,0%,55%);
  cursor: default;
  margin: 0;
}
:scope .thumb:active {
  box-shadow: inset 0 var(--thumb-size) #0002;
}
:scope .thumb-min {
  left: calc(-1px - var(--thumb-radius));
}
:scope .thumb-max {
  right: calc(-1px - var(--thumb-radius));
}
`


function rangeInput(options = {}) {
  const {
    min = 0,
    max = 100,
    step = 'any',
    value: defaultValue = [min, max],
    color,
    width,
    theme = theme_Flat,
  } = options;
  
  const controls = {};
  const scope = randomScope();
  const clamp = (a, b, v) => v < a ? a : v > b ? b : v;
  const html = htl.html;

  // Will be used to sanitize values while avoiding floating point issues.
  const input = html`<input type=range ${{min, max, step}}>`;
  
  const dom = html`<div class=${`${scope} range-slider`} style=${{
    color,
    width: cssLength(width),
  }}>
  ${controls.track = html`<div class="range-track">
    ${controls.zone = html`<div class="range-track-zone">
      ${controls.range = html`<div class="range-select" tabindex=0>
        ${controls.min = html`<div class="thumb thumb-min" tabindex=0>`}
        ${controls.max = html`<div class="thumb thumb-max" tabindex=0>`}
      `}
    `}
  `}
  ${html`<style>${theme.replace(/:scope\b/g, '.'+scope)}`}
</div>`;

  let value = [], changed = false;
  Object.defineProperty(dom, 'value', {
    get: () => [...value],
    set: ([a, b]) => {
      value = sanitize(a, b);
      updateRange();
    },
  });

  const sanitize = (a, b) => {
    a = isNaN(a) ? min : ((input.value = a), input.valueAsNumber);
    b = isNaN(b) ? max : ((input.value = b), input.valueAsNumber);
    return [Math.min(a, b), Math.max(a, b)];
  }
  
  const updateRange = () => {
    const ratio = v => (v - min) / (max - min);
    dom.style.setProperty('--range-min', `${ratio(value[0]) * 100}%`);
    dom.style.setProperty('--range-max', `${ratio(value[1]) * 100}%`);
  };

  const dispatch = name => {
    dom.dispatchEvent(new Event(name, {bubbles: true}));
  };
  const setValue = (vmin, vmax) => {
    const [pmin, pmax] = value;
    value = sanitize(vmin, vmax);
    updateRange();
    // Only dispatch if values have changed.
    if(pmin === value[0] && pmax === value[1]) return;
    dispatch('input');
    changed = true;
  };
  
  setValue(...defaultValue);
  
  // Mousemove handlers.
  const handlers = new Map([
    [controls.min, (dt, ov) => {
      const v = clamp(min, ov[1], ov[0] + dt * (max - min));
      setValue(v, ov[1]);
    }],
    [controls.max, (dt, ov) => {
      const v = clamp(ov[0], max, ov[1] + dt * (max - min));
      setValue(ov[0], v);
    }],
    [controls.range, (dt, ov) => {
      const d = ov[1] - ov[0];
      const v = clamp(min, max - d, ov[0] + dt * (max - min));
      setValue(v, v + d);
    }],
  ]);
  
  // Returns client offset object.
  const pointer = e => e.touches ? e.touches[0] : e;
  // Note: Chrome defaults "passive" for touch events to true.
  const on  = (e, fn) => e.split(' ').map(e => document.addEventListener(e, fn, {passive: false}));
  const off = (e, fn) => e.split(' ').map(e => document.removeEventListener(e, fn, {passive: false}));
  
  let initialX, initialV, target, dragging = false;
  function handleDrag(e) {
    // Gracefully handle exit and reentry of the viewport.
    if(!e.buttons && !e.touches) {
      handleDragStop();
      return;
    }
    dragging = true;
    const w = controls.zone.getBoundingClientRect().width;
    e.preventDefault();
    handlers.get(target)((pointer(e).clientX - initialX) / w, initialV);
  }
  
  
  function handleDragStop(e) {
    off('mousemove touchmove', handleDrag);
    off('mouseup touchend', handleDragStop);
    if(changed) dispatch('change');
  }
  
  invalidation.then(handleDragStop);
  
  dom.ontouchstart = dom.onmousedown = e => {
    dragging = false;
    changed = false;
    if(!handlers.has(e.target)) return;
    on('mousemove touchmove', handleDrag);
    on('mouseup touchend', handleDragStop);
    e.preventDefault();
    e.stopPropagation();
    
    target = e.target;
    initialX = pointer(e).clientX;
    initialV = value.slice();
  };
  
  controls.track.onclick = e => {
    if(dragging) return;
    changed = false;
    const r = controls.zone.getBoundingClientRect();
    const t = clamp(0, 1, (pointer(e).clientX - r.left) / r.width);
    const v = min + t * (max - min);
    const [vmin, vmax] = value, d = vmax - vmin;
    if(v < vmin) setValue(v, v + d);
    else if(v > vmax) setValue(v - d, v);
    if(changed) dispatch('change');
  };
  
  return dom;
}



function randomScope(prefix = 'scope-') {
  return prefix + (performance.now() + Math.random()).toString(32).replace('.', '-');
}

cssLength = v => v == null ? null : typeof v === 'number' ? `${v}px` : `${v}`

