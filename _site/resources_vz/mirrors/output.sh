# To safely create the mirror files review the commands below before running !
# Also, don't commit HAR files to Git providers/npm !


mkdir -p npm/@observablehq/inputs@0.10.4/dist
curl https://cdn.jsdelivr.net/npm/@observablehq/inputs@0.10.4/dist/inputs.min.js > npm/@observablehq/inputs@0.10.4/dist/inputs.min.js
mkdir -p npm/marked@0.3.12
curl https://cdn.jsdelivr.net/npm/marked@0.3.12/marked.min.js > npm/marked@0.3.12/marked.min.js
mkdir -p npm/@observablehq/graphviz@0.2.1/dist
curl https://cdn.jsdelivr.net/npm/@observablehq/graphviz@0.2.1/dist/graphviz.min.js > npm/@observablehq/graphviz@0.2.1/dist/graphviz.min.js
mkdir -p npm/vega
curl https://cdn.jsdelivr.net/npm/vega/package.json > npm/vega/package.json
mkdir -p npm/compassql
curl https://cdn.jsdelivr.net/npm/compassql/package.json > npm/compassql/package.json
mkdir -p gh/uwdata/gemini@v0.1-alpha
curl https://cdn.jsdelivr.net/gh/uwdata/gemini@v0.1-alpha/gemini.web.js > gh/uwdata/gemini@v0.1-alpha/gemini.web.js
mkdir -p npm/json2csv@5.0.7/dist
curl https://cdn.jsdelivr.net/npm/json2csv@5.0.7/dist/json2csv.umd.js > npm/json2csv@5.0.7/dist/json2csv.umd.js
mkdir -p npm/htl@0.3.1/dist
curl https://cdn.jsdelivr.net/npm/htl@0.3.1/dist/htl.min.js > npm/htl@0.3.1/dist/htl.min.js
mkdir -p npm/d3
curl https://cdn.jsdelivr.net/npm/d3/package.json > npm/d3/package.json
mkdir -p npm/vega-lite
curl https://cdn.jsdelivr.net/npm/vega-lite/package.json > npm/vega-lite/package.json
mkdir -p npm/lodash@4.17.21
curl https://cdn.jsdelivr.net/npm/lodash@4.17.21/lodash.min.js > npm/lodash@4.17.21/lodash.min.js
mkdir -p npm/@observablehq/highlight.js@2.0.0
curl https://cdn.jsdelivr.net/npm/@observablehq/highlight.js@2.0.0/highlight.min.js > npm/@observablehq/highlight.js@2.0.0/highlight.min.js
mkdir -p npm/vega@5.25.0/build
curl https://cdn.jsdelivr.net/npm/vega@5.25.0/build/vega.min.js > npm/vega@5.25.0/build/vega.min.js
mkdir -p npm/d3@7.8.5/dist
curl https://cdn.jsdelivr.net/npm/d3@7.8.5/dist/d3.min.js > npm/d3@7.8.5/dist/d3.min.js
mkdir -p npm/vega-lite@5.9.3/build
curl https://cdn.jsdelivr.net/npm/vega-lite@5.9.3/build/vega-lite.min.js > npm/vega-lite@5.9.3/build/vega-lite.min.js
mkdir -p npm/compassql@0.21.2/build
curl https://cdn.jsdelivr.net/npm/compassql@0.21.2/build/compassql.min.js > npm/compassql@0.21.2/build/compassql.min.js
mkdir -p npm/vega-lite@5.9.3
curl https://cdn.jsdelivr.net/npm/vega-lite@5.9.3/package.json > npm/vega-lite@5.9.3/package.json
mkdir -p npm/vega@^5.24.0
curl https://cdn.jsdelivr.net/npm/vega@^5.24.0/package.json > npm/vega@^5.24.0/package.json
mkdir -p npm/vega-embed
curl https://cdn.jsdelivr.net/npm/vega-embed/package.json > npm/vega-embed/package.json
mkdir -p npm/vega-embed@6.22.1/build
curl https://cdn.jsdelivr.net/npm/vega-embed@6.22.1/build/vega-embed.min.js > npm/vega-embed@6.22.1/build/vega-embed.min.js
mkdir -p npm/vega-embed@6.22.1
curl https://cdn.jsdelivr.net/npm/vega-embed@6.22.1/package.json > npm/vega-embed@6.22.1/package.json
mkdir -p npm/vega@^5.21.0
curl https://cdn.jsdelivr.net/npm/vega@^5.21.0/package.json > npm/vega@^5.21.0/package.json
mkdir -p npm/vega-lite@*
curl https://cdn.jsdelivr.net/npm/vega-lite@*/package.json > npm/vega-lite@*/package.json


# To safely create the mirror files review the commands above before running !
# Also, don't commit HAR files to Git providers/npm !
