
const fs = require('fs');
const path = require('path');

let har_json = fs.readFileSync('har.json');
let har = JSON.parse(har_json);

console.log("# To safely create the mirror files review the commands below before running !")
console.log("# Also, don't commit HAR files to Git providers/npm !\n\n")

for (var i = 0; i < har.log.entries.length; i++) {
  let url = new URL(decodeURI(har.log.entries[i].request.url));
  let pathname = url.pathname.substring(1);
  console.log('mkdir -p ' + path.dirname(pathname));
  console.log('curl ' + url.href + ' > ' + pathname)
}

console.log("\n\n# To safely create the mirror files review the commands above before running !")
console.log("# Also, don't commit HAR files to Git providers/npm !")

//console.log(har.log.entries[0].response.content.text)